import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class AllLocators {
	static String url = "file:///D:/Seleniumclasses/workspace/SeleniumAutomationProject/LocatingMultipleElements.html";
	public static void main(String[] args) {
		System.setProperty("webdriver.firefox.marionette", "D:/Seleniumclasses/workspace/SeleniumAutomationProject/drivers/geckodriver.exe");
		 WebDriver driver = new FirefoxDriver();
		 driver.get(url);
		 WebElement ww= driver.findElement(By.cssSelector("#fname"));
		 ww.clear();
		 ww.sendKeys("Hello123");
		 WebElement wq= driver.findElement(By.cssSelector("#lname"));
		 wq.clear();
		 wq.sendKeys("Hi123");
		 WebElement wg= driver.findElement(By.cssSelector("#designation"));
		 Select dropdown=new Select(wg);
		 dropdown.selectByVisibleText("CEO");
		 WebElement wc= driver.findElement(By.cssSelector("#designation2"));
		 Select dropdownv=new Select(wc);
		 dropdownv.selectByVisibleText("Java");
		 WebElement wz= driver.findElement(By.xpath("html/body/form/fieldset/div[4]/p[2]/input"));
		 wz.click();
	}

}
