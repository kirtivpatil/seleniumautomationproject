import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PopUp {
	static String popurl = "http://demo.guru99.com/popup.php";
	
	public static void main(String[] args) {
		System.setProperty("webdriver.firefox.marionette", "D:/Seleniumclasses/workspace/SeleniumAutomationProject/drivers/geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get(popurl);
	
	driver.findElement(By.linkText("Click Here")).click();
	
	String parent = driver.getWindowHandle();//capture parent window
	Set<String> windows = driver.getWindowHandles(); //capture all child windows
	
	Iterator<String> iter = windows.iterator();
	
	while(iter.hasNext()){
		String window = iter.next();
		
		if(!parent.equals(window)){
			driver.switchTo().window(window);
			
			driver.findElement(By.name("btnLogin")).click();
			driver.close();
		}

}
	driver.switchTo().window(parent);
	String title = driver.getTitle();
	System.out.println(title);
	}
	
	
}
