import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Frames {

	static String frameurl = "file:///D:/Seleniumclasses/workspace/ParentFrame.html";
	public static void main(String[] args) {
		
		System.setProperty("webdriver.firefox.marionette", "D:/Seleniumclasses/workspace/SeleniumAutomationProject/drivers/geckodriver.exe");
		WebDriver driver = new FirefoxDriver();
		driver.get(frameurl);
		driver.switchTo().frame("top");
		String title = driver.getPageSource();
		System.out.println(title);

		//driver.switchTo().frame(0); //top is same as Frame(0). see docuemntation for different ways
		//String title1 = driver.getTitle();
		//System.out.println(title1);
		driver.switchTo().defaultContent();
		//String titledefault = driver.getPageSource();
		//System.out.println(titledefault);

	}

}
