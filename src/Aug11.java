import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.TargetLocator;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Aug11 {
	
	static String alerturl = "file:///D:/Seleniumclasses/workspace/Alert.html";
	
	public static void main(String[] args) {
		
		
		System.setProperty("webdriver.firefox.marionette", "D:/Seleniumclasses/workspace/SeleniumAutomationProject/drivers/geckodriver.exe");
		 WebDriver driver = new FirefoxDriver();
		 driver.get(alerturl);
		 WebElement wd= driver.findElement(By.cssSelector("#simple"));
		 wd.click();
		 TargetLocator target = driver.switchTo();
		 Alert al= target.alert(); //see documentation to understand more
		 al.accept();
		 

	}

}
